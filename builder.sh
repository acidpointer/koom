#!/usr/bin/env bash

# Builder
# Sets up build directories
# Creates compile_commands.json for ccls/clangd


# Requirements: (Fedora)
# 


SCRIPT="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "$SCRIPT")"

CMAKE="/usr/bin/cmake"
MAKE="/usr/bin/make"
NINJA="/usr/bin/ninja"

MAKE_COMPILE_COMMANDS="$CMAKE -H. -BDebug -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=YES"

function msg() {
    echo -e "$1"
}

function check_binary() {
    command -v "$1" >/dev/null 2>&1
}

function compile_commands() {
    cd "$DIR"
    if [[ ! -f "$DIR/CMakeLists.txt" ]]; then
	msg "CMakeLists.txt not found!"
	return
    fi
    
    if [[ ! -f "$DIR/compile_commands.json" ]]; then
       msg "Preparing compile_commands.json ..."
       $MAKE_COMPILE_COMMANDS
       ln -s "$DIR/Debug/compile_commands.json" .
    else
	rm "$DIR/compile_commands.json"
	msg "Recreating compile_commands.json"
	compile_commands
    fi
}

function build () {
    local build_type=
    if check_binary $CMAKE; then
        echo "Cmake found!"
        if check_binary "$NINJA"; then
            echo "Ninja found!"
            mkdir -p "$DIR/build-ninja-$1"
            cd "$DIR/build-ninja-$1"
            $CMAKE -G "Ninja" "$DIR" -DCMAKE_BUILD_TYPE="$1"
            $NINJA
            return;
        fi
        if check_binary "$MAKE"; then
            echo "Make found!"
        fi
    fi
}


compile_commands
